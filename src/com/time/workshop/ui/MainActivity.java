package com.time.workshop.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.time.workshop.R;
import com.time.workshop.injector.Injector;
import com.time.workshop.injector.V;
import com.time.workshop.ui.fragment.HeadFragment;
import com.time.workshop.ui.fragment.MenuFragment;
import com.time.workshop.ui.fragment.MyFragment;
import com.time.workshop.ui.fragment.WorkShopFragment;

public class MainActivity extends SlidingFragmentActivity {
	public SlidingMenu menu;
	public View rootView;
	private HeadFragment headFragment;
	private WorkShopFragment workShopFragment;
	private MyFragment myFragment;
	private MenuFragment menuFragment;
	
	@V
	private Button btn_fragment_left,btn_fragment_right;
	@V
	private ImageView img_shadow;

    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseView.setTitleView(this, R.layout.base_view_titleleft,
				R.layout.base_view_titlecenter_with_logo,
				R.layout.base_view_titleright_main,
				R.layout.activity_slidingmenu);
		rootView = baseView.getTitleView();
		setContentView(rootView);
		setView();
    }
    
    private void setView(){
    	Injector.getInstance().inject(this);
    	if (null == headFragment) {
			headFragment = new HeadFragment();
		}
    	if (null == workShopFragment) {
    		workShopFragment = new WorkShopFragment();
    	}
    	if (null == myFragment) {
    		myFragment = new MyFragment();
    	}
    	if (null == menuFragment) {
    		menuFragment = new MenuFragment();
    	}
    	btn_fragment_left
		.setBackgroundResource(R.drawable.btn_title_left_bg_selector);
    	btn_fragment_left.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu.showMenu();
			}
		});
    	menu = getSlidingMenu();
		menu.setMode(SlidingMenu.LEFT);// 设置侧边栏菜单为左模式
		menu.setBehindWidthRes(R.dimen.width_30_80);// 设置左边菜单的宽度,该值为左菜单展开的宽度
		menu.setShadowDrawable(R.drawable.shadow);// 设置左菜单的阴影
		menu.setShadowWidth(10);// 设置阴影宽度
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);// 设置侧边栏菜单触摸模式为全屏模式
		setBehindContentView(R.layout.slidingmenu); // 设置左菜单的默认VIEW
		
		getSupportFragmentManager().beginTransaction()
		.add(R.id.menu_frame, menuFragment)
		.replace(R.id.basecontent, headFragment).commit(); // 将左菜单默认VIEW替换为左菜单Fragment
		menu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
			@Override
			public void onClose() {
				img_shadow.setVisibility(View.GONE);
				btn_fragment_left
						.setBackgroundResource(R.drawable.base_icon_left_menu);
			}
		});
		menu.setOnOpenListener(new SlidingMenu.OnOpenListener() {
			@Override
			public void onOpen() {
				img_shadow.setVisibility(View.GONE);
				btn_fragment_left
						.setBackgroundResource(R.drawable.base_icon_left_menu);
			}
		});
    }
    
    public void showContent() {
		if (menu.isMenuShowing()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					menu.toggle(false);
				}
			}, 50);
		}
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
