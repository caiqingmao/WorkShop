package com.time.workshop;

import java.util.ArrayList;
import java.util.List;



public class BaseConstant {
	
	public static final String SUCCESS = "1";
	//销售线索列表
	public static final String XSXS = "销售线索";
	public static final String WDXS = "我的线索";
	public static final String WFPDXS = "未分配的线索";
	public static final String SYXS = "所有线索";
	//客户列表
	public static final String KH = "客户";
	public static final String WDKH = "我的客户";
	public static final String WFPDKH = "未分配的客户";
	public static final String SYKH = "所有客户";
	//联系人
	public static final String LXR = "联系人";
	public static final String WDLXR = "我的联系人";
	public static final String SYLXR = "所有联系人";
	
	public static long startTime = 0;
	public static long endTime = 0;
	
	
	public static final boolean SHOWJOKE = true;
	public static final int PAGE = 1; // 第一页
	public static final int PAGE_NUM = 20; // 分页的数量
	
	

	

}
