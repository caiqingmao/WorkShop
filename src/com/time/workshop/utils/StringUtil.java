package com.time.workshop.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextUtils;
import android.widget.TextView;

import com.time.workshop.App;


public class StringUtil {

	public static String getTextViewContent(TextView textView ) {
		return textView.getText().toString().trim();
	}


	/**
	 * 验证字符串是否为空，若为空就弹出 Toast，内容为  errorStringRes
	 * @param content
	 * @param errorStringRes
	 * @return true 验证未通过，false 验证通过
	 */
	public static boolean invalidateContent(String content, int errorStringRes) {
		if( TextUtils.isEmpty(content) ) {
			if( errorStringRes > 0 ) {
				App.showShortToast(App.getInstance().getString(errorStringRes));
			}
			return true;
		}
		return false;
	}

	/**
	 * 字符串是否为手机号
	 * @param phoneNo
	 * @return true 是 手机号， false 不是 手机号
	 */
	public static boolean isPhoneNo(String phoneNo) {
		return regxStr(phoneNo, "^(\\+86)?0?1[3|4|5|8|7]\\d{9}$");
	}

	/**
	 * 校验是否为手机号
	 * @param text
	 * @return true 是 手机号， false 不是 手机号
	 */
	public static boolean validatePhoneNo( TextView text ) {
		return isPhoneNo( getTextViewContent(text) );
	}


	/**
	 * 正则匹配
	 * @param validateStr	待校验的字符串
	 * @param pattern	正则表达式
	 * @return
	 */
	public static boolean regxStr( String validateStr, String pattern ) {
		Pattern p = Pattern.compile(pattern);
		return p.matcher(validateStr).matches();
	}


	/**
	 *  验证是否为正确的邮箱
	 * @param email
	 * @return  true 为正确的邮箱， false 反之
	 */
	public static boolean isEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);
		return m.matches();
	}
	
	/**
	 *  验证是否是正确的邮编
	 * @param zipcode
	 * @return true  false
	 */
	public static boolean isZipCode(String zipcode){
		String  str = "[1-9]\\d{5}(?!\\d)";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(zipcode);
		return m.matches();
	}


}
