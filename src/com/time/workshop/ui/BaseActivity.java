package com.time.workshop.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.Window;
import android.widget.TextView;

import com.time.workshop.ExitManager;
import com.time.workshop.utils.StringUtil;

public class BaseActivity extends FragmentActivity{

	public final String TAG = getClass().getSimpleName();

//	private LoadingDialog mLoadingDialog;


	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//将activity添加到activity管理，方便退出
		ExitManager.getInstance().addActivity(this);

	}


	/**
	 * 从 Intent 中取出存储的值。{@link #restoreExtras(Bundle)}
	 * @param intent
	 * @return
	 */
	protected boolean restoreExtras(final Intent intent) {
		if( intent == null ) {
			return false;
		}
		return restoreExtras(intent.getExtras());
	}

	/**
	 * 取得 bundle 中存储的值.
	 * <p>NOTE : 若要使用该方法，应该在子类中覆写此方法
	 * @param extra 
	 * @return true 成功取值  | false 取值不成功
	 */
	protected boolean restoreExtras(final Bundle extra) {
		return false;
	}

	/**
	 * bundle 是否为空
	 * @param extra
	 * @return true 为空 | false 不成功
	 */
	protected boolean isBundleEmpty(Bundle extra) {
		return extra == null || extra.isEmpty();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}


	/**
	 * 显示正在加载的进度框
	 */
	public void showLoadingDialog(String msg) {
		dismissAllDialog();
//		mLoadingDialog = new LoadingDialog(this);
//		mLoadingDialog.showDialog(msg);
	}
	/**
	 * 显示正在加载的进度框
	 */
	public void showLoadingDialog(String msg,boolean isCancelable) {
		dismissAllDialog();
//		mLoadingDialog = new LoadingDialog(this,isCancelable);
//		mLoadingDialog.showDialog(msg);
	}

	/**
	 * 卸载加载进度框
	 */
	public void dismissLoadingDialog() {
//		AbsCustomAlertDialog.dismissDialog(mLoadingDialog);
	}

	/**
	 * 卸载错误对话框
	 */
	protected void dismissMessageDialog() {
		//		AbsCustomAlertDialog.dismissDialog(mMessageDialog);
	}

	/**
	 * 卸载所有对话框
	 */
	protected void dismissAllDialog() {
		dismissLoadingDialog();
		dismissMessageDialog();
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		dismissLoadingDialog();

	}

	/**
	 * 验证 TextView 中的内容是否为空，若为空就弹出 Toast，内容为  errorStringRes
	 * @param textView
	 * @param errorStringRes
	 * @return true 验证未通过，false 验证通过
	 */
	protected boolean invalidateText(TextView textView, int errorStringRes) {
		return StringUtil.invalidateContent(getText(textView), errorStringRes);
	}

	/**
	 * 验证 TextView 中的内容是否为空
	 * @param textView
	 * @param errorStringRes
	 * @return true 验证未通过，false 验证通过
	 */
	protected boolean invalidateText(TextView textView) {
		return TextUtils.isEmpty(getText(textView));
	}

	protected String getText( TextView textView ) {
		return textView.getText().toString();
	}

}
