package com.time.workshop.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.time.workshop.R;

public class SplashActivity extends BaseActivity implements Runnable{
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_splash);
		
		new Handler().postDelayed(this, 2 * 1000);
	}

	@Override
	public void run() {
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

}
