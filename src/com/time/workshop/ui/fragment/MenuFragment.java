package com.time.workshop.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.time.workshop.BaseFragment;
import com.time.workshop.R;
import com.time.workshop.injector.Injector;
import com.time.workshop.injector.V;
import com.time.workshop.ui.MainActivity;


/**
 * Created by cheng on 14-3-31. 左侧边栏
 */
public class MenuFragment extends BaseFragment implements OnClickListener{

	@V
	private TextView head,workshop,my;
	@V
	private Button btn_fragment_left;
	@V
	private ImageView img_fragment_title;
	
	private View rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
//		mSuperActivity.baseView.setTitleView(getActivity(), R.layout.base_view_titleleft,
//				R.layout.base_view_titlecenter_with_logo, 0,
//				R.layout.fragment_menu);
//		rootView = mSuperActivity.baseView.getTitleView();
//		return rootView;
		return inflater.inflate(R.layout.fragment_menu, container, false);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Injector.getInstance().inject(getActivity(), this, view);
//		img_fragment_title.setVisibility(View.GONE);
//		btn_fragment_left.setVisibility(View.GONE);
//		txt_fragment_title.setVisibility(View.VISIBLE);
//		txt_fragment_title.setText("splendidcrm");
		head.setOnClickListener(this);
		workshop.setOnClickListener(this);
		my.setOnClickListener(this);

	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.head:
			addMenu(HeadFragment.class);
			break;
		case R.id.workshop:
			addMenu(WorkShopFragment.class);
			break;
		case R.id.my:
			addMenu(MyFragment.class);
			break;
		default:
			break;
		}
	}
	
	public void addMenu(Class<? extends Fragment> clazz) {
		try {
			try {
				
				mSuperActivity.popBackAllStack();
			} catch (Exception e) {
				e.printStackTrace();
			}
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			// ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.setCustomAnimations(0, 0, 0, R.anim.fade_out);
			ft.addToBackStack(null);
			Fragment fragment = clazz.newInstance();
			ft.replace(R.id.basecontent, fragment).commitAllowingStateLoss();
			((MainActivity) getActivity()).showContent();
		} catch (java.lang.InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	
}
