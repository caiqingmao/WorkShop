package com.time.workshop;


import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.time.workshop.utils.ScreenSizeUtil;

/**
 * 
 * @author wangfeng
 *
 */
public class App extends Application {

	private final static String TAG = "com.time.workshokp";
	public final static boolean DEBUG = true;
	public final static double imgrote = 0.40;
	public boolean isReceiveMSG = false;
	public static Context applicationContext;
	private static App mInstance = null;
	private LocationClient mLocationClient;
	public MyLocationListener mMyLocationListener;
	private int i = 0;
	public boolean isFrist = false;

	private ScreenSizeUtil sizeUtil;

	public ScreenSizeUtil getSizeUtil() {
		return sizeUtil;
	}

	public void setSizeUtil(ScreenSizeUtil sizeUtil) {
		this.sizeUtil = sizeUtil;
	}

	public int getScreenWidth() {
		return sizeUtil.getScreenWidth();
	}


	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		sizeUtil = new ScreenSizeUtil(this);
		mLocationClient = new LocationClient(this.getApplicationContext());
		mMyLocationListener = new MyLocationListener();
		mLocationClient.registerLocationListener(mMyLocationListener);
		InitLocation();
		mLocationClient.start();
	}

	/**定位参数设置*/
	private void InitLocation(){
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);//设置定位模式
		option.setCoorType("bd09ll");//返回的定位结果是百度经纬度，默认值gcj02
		int span=2000;
		option.setScanSpan(span);//设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
	}

	/**
	 * 实现实位回调监听
	 */
	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (!isFrist) {
				if (location.getAddrStr() != null) {
					isFrist = true;
					i = i+1;
					System.out.println("第"+i+"次，地址："+location.getAddrStr());
				}
			}
		}


	}



	public static App getInstance() {

		if(mInstance == null ){
			mInstance = new App();
		}
		return mInstance;

	}
	//--Toast--------------------------------
	private static Handler mHandler = new Handler();
	private static Toast toast;
	public static void showShortToast(final CharSequence msg) {
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				if (toast == null)
					toast = Toast.makeText(mInstance, msg, Toast.LENGTH_SHORT);
				else
					toast.setText(msg);

				toast.show();
			}
		});
	}
	public static void showShortToast(int resId) {
		showShortToast(mInstance.getString(resId));
	}

	/**
	 *打印Logs
	 * 
	 * @param msg
	 */
	public static void showLog(Object msg) {
		if (DEBUG)
			Log.i("BAO", msg + "");
	}

	public static void debug(String tag, Object msg) {
		if (DEBUG)
			Log.d(TextUtils.isEmpty(tag) ? TAG : tag, String.valueOf(msg));
	}

	public static void debug(Object msg) {
		debug(null, msg);
	}

}
